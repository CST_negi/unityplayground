﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using System;
using UnityEngine.UI;

public class RealtimeDBTest : MonoBehaviour
{

    enum Place { SmartPhone, PC }
    Place place;
    public Text debugText;

    DependencyStatus dependencyStatus = DependencyStatus.UnavailableOther;

    void Start()
    {
        dependencyStatus = FirebaseApp.CheckDependencies();
        if (dependencyStatus != DependencyStatus.Available)
        {
            FirebaseApp.FixDependenciesAsync().ContinueWith(task =>
            {
                dependencyStatus = FirebaseApp.CheckDependencies();
                if (dependencyStatus == DependencyStatus.Available)
                {
                    InitializeFirebase();
                }
                else
                {
                    Debug.LogError(
                        "Could not resolve all Firebase dependencies: " + dependencyStatus);
                }
            });
        }
        else
        {
            InitializeFirebase();
        }
    }

    private void InitializeFirebase()
    {
        var instance = FirebaseApp.DefaultInstance;
        instance.SetEditorDatabaseUrl("https://poyoc-home-agent.firebaseio.com/");

        if (instance.Options.DatabaseUrl != null)
        {
            instance.SetEditorDatabaseUrl(instance.Options.DatabaseUrl);
        }

        var refer = FirebaseDatabase.DefaultInstance.GetReference("Place");
        refer.ChildChanged += PlaceChanged;
        refer.GetValueAsync().ContinueWith(x =>
        {
            if (x.Result == null) { Debug.Log("null"); }
            else
            {
                //debugText.text = x.Result.ToString();
            }
        });
    }

    private void PlaceChanged(object sender, ChildChangedEventArgs e)
    {
        Debug.Log("PlaceChanged");

        if (e.DatabaseError != null)
        {
            Debug.LogError(e.DatabaseError.Message);
            return;
        }
        var val = e.Snapshot.Value;
        debugText.text = val.ToString();

        Debug.Log(val);
    }

    public void AddScore()
    {
        DatabaseReference reference = FirebaseDatabase.DefaultInstance.GetReference("Place");
        // Use a transaction to ensure that we do not encounter issues with
        // simultaneous updates that otherwise might create more than MaxScores top scores.
        reference.RunTransaction(AddScoreTransaction)
          .ContinueWith(task =>
          {
              if (task.Exception != null)
              {
                  Debug.Log(task.Exception.ToString());
              }
              else if (task.IsCompleted)
              {
                  Debug.Log("Transaction complete.");
              }
          });
    }

    TransactionResult AddScoreTransaction(MutableData mutableData)
    {
        Dictionary<string, object> newScoreMap = new Dictionary<string, object>();
        newScoreMap["p"] = "おちんちんホールディングス";

        // You must set the Value to indicate data at that location has changed.
        mutableData.Value = newScoreMap;//leaders;
        return TransactionResult.Success(mutableData);
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            AddScore();
        }
    }
}
