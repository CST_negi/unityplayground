﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UEPub;

public class EbookRenderer : MonoBehaviour {
	public Text displayText;
	public TextAsset testBook;

	//private EpubBook epubBook;

	// Use this for initialization
	void Start () {
		OpenEbookFile ();
	}
	
	void OpenEbookFile(){
        // Opening a book
        //epubBook = EpubReader.OpenBook("austen-pride-and-prejudice-illustrations.epub");
        //EpubChapter chapter = epubBook.Chapters [0];
        //displayText.text = chapter.HtmlContent;
        Debug.Log(Application.streamingAssetsPath + "/EpubReader/" + "chumonno_oi_ryoriten.epub");
        var epub = new UEPubReader("file:///" + Application.streamingAssetsPath + "/EpubReader/" +  "chumonno_oi_ryoriten.epub");

		Debug.Log (epub.epubFolderLocation);

		displayText.text = epub.chapters[10];
	}
}
